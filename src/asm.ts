import {TreeNode} from './NodeType'


class VarInfo {
    type: VarType;
    location: string;
    line: number;
    constructor(t: VarType, location: string, line: number){
        this.type = t;
        this.location = location;
        this.line = line;
    }
}

class SymbolTable {
    table: Map<string, VarInfo>;
    constructor(){
        this.table = new Map();
    }
    get(name: string) {
        if(!this.table.has(name))
            throw new Error(`${name} does not exist.` );
        return this.table.get(name);
    }
    set(name: string, v: VarInfo) {
        if(this.table.has(name))
            throw new Error(`Redeclaration of variable ${name}.`);
        this.table.set(name, v);
    }
    has(name: string) {
        return this.table.has(name);
    }
}


let asmCode: string[] = [];
let symTable: SymbolTable = new SymbolTable();
let stringPool: Map<string, string> = new Map();
let labelCounter = 0;

enum VarType {
    INTEGER,
    FLOAT, 
    STRING,
    VOID,
}

function label() {
    let s = "lbl" + labelCounter++;
    return s;
}

function ICE() {
    throw new Error("Internal Compiler Error");
}

export function makeAsm( root: TreeNode ){
    asmCode = [];
    symTable = new SymbolTable();
    stringPool = new Map();
    labelCounter = 0;
    emit("default rel");
    emit("section .text");
    emit("%include \"doCall.asm\"");
    emit("global main");
    emit("main:");
    emit("mov arg0, 0");
    emit("mov arg1, string_r");
    emit("ffcall fdopen");
    emit("mov [stdin], rax");
    emit("mov arg0, 1");
    emit("mov arg1, string_w");
    emit("ffcall fdopen");
    emit("mov [stdout], rax");
    programNodeCode(root);
    emit("ret");
    emit("section .data");
    emit("stdin: dq 0");
    emit("stdout: dq 0");
    emit("string_r: db 'r', 0");
    emit("string_w: db 'w', 0");
    emit("string_a: db 'a',0");
    emit("string_rplus: db 'r+',0");
    emit("string_percent_s: db '%s',0");
    emit("string_percent_d: db '%d',0");
    emit("fgets_buffer: times 64 db 0");
    outputSymbolTableInfo();
    outputStringPoolInfo();

    return asmCode.join("\n");
}

function emit( instr: string) {
    asmCode.push(instr);
}

function outputSymbolTableInfo(){
    for(let vname of symTable.table.keys() ){
        let vinfo = symTable.get(vname);
        emit( `${vinfo.location}:`);
        emit( "dq 0" );
    }
}

function outputStringPoolInfo(){
    for(let key of stringPool.keys() ){
        let lbl = stringPool.get(key);
        emit( `${lbl}:` );
        for(let i=0;i<key.length;++i){
            if(key.charAt(i) == '\n')
                emit("db 10");
            else
                emit(`db ${key.charCodeAt(i)}`);
        }
        emit("db 0");   //null terminator
    }
}

function programNodeCode(n: TreeNode) {
    //program -> braceblock
    if(n.sym != "program"){
        ICE();
    }
    varDeclListNodeCode(n.children[0]);
    braceblockNodeCode(n.children[1]);
}

function assignNodeCode(n: TreeNode) {
    // assign : ID EQ expr
    let expr_type: VarType = exprNodeCode(n.children[2]);
    let v_name: string = n.children[0].token.lexeme;
    if(symTable.get(v_name).type !== expr_type)
        throw new Error(`Type mismatch: ${symTable.get(v_name).type} != ${expr_type}.`)
    moveBytesFromStackToLocation(symTable.get(v_name).location);
}

function stringconstantNodeCode( n: TreeNode ){
    let s = n.token.lexeme;
    s = s.substring(1, s.length - 1);
    let return_str = '';
    for(let i = 0; i < s.length; i++) {
        if(s.charAt(i) == '\\' && i + 1 < s.length && s.charAt(i + 1) == 'n')
        {
            return_str += '\n'
            i += 2;
        }
        if(s.charAt(i) == '\\' && i + 1 < s.length)
        {
            return_str += s.charAt(i + 1);
            i++;
        }
        else
            return_str += s.charAt(i);
    }
    if( !stringPool.has( return_str ) )
        stringPool.set( return_str, label() );
    return stringPool.get(return_str);   //return the label
}

function mulVarDecl(n: TreeNode, type: VarType) {
    if(n.children.length === 0)
        return;
    let v_name = n.children[1].token.lexeme;
    let v_line = n.children[1].token.line;
    symTable.set(v_name, new VarInfo(type, label(), v_line));
    mulVarDecl(n.children[2], type)
}

function varDeclNodeCode(n: TreeNode) {
    // varDecl : TYPE ID mulVarDecl | TYPE assign;
    let v_type = typeNodeCode(n.children[0]);
    if(n.children.length === 3){
        let v_name = n.children[1].token.lexeme;
        let v_line = n.children[1].token.line;
        symTable.set(v_name, new VarInfo(v_type, label(), v_line));
        mulVarDecl(n.children[2], v_type);
    }
    else {
        let assign = n.children[1];
        let assign_id = assign.children[0].token.lexeme;
        let assign_line = assign.children[0].token.line;
        symTable.set(assign_id, new VarInfo(v_type, label(), assign_line));
        assignNodeCode(n.children[1]);
    }
}

function varDeclListNodeCode(n: TreeNode) {
    // varDeclList : varDecl SEMI varDeclList | ;
    if(n.children.length === 0)
        return;
    varDeclNodeCode(n.children[0]);
    varDeclListNodeCode(n.children[2]);
}

function typeNodeCode(n: TreeNode) {
    if(n.token.sym !== "TYPE")
        ICE();
    switch(n.token.lexeme){
        case "int":
            return VarType.INTEGER;
        case "double":
            return VarType.FLOAT;
        case "string":
            return VarType.STRING;
        default:
            ICE();
    }
}

function braceblockNodeCode(n: TreeNode) {
    //braceblock -> LBR stmts RBR
    stmtsNodeCode(n.children[1]);
}

function stmtsNodeCode(n: TreeNode) {
    //stmts -> stmt stmts | lambda
    if(n.children.length == 0){
        return;
    }
    stmtNodeCode(n.children[0]);
    stmtsNodeCode(n.children[1]);
}

function stmtNodeCode(n: TreeNode) {
    //stmt -> cond | loop | return-stmt SEMI
    let c = n.children[0]
    switch(c.sym){
        case "cond":
            condNodeCode(c);
            break;
        case "loop":
            loopNodeCode(c);
            break;
        case "returnStmt":
            returnStmtNodeCode(c);
            break;
        case "assign":
            assignNodeCode(c);
            break;
        case "funcCall":
            funcCallNodeCode(c);
            break;
        default:
            ICE();
    }
}

function condNodeCode(n: TreeNode) {
    //cond -> IF LP expr RP braceblock | IF LP expr RP braceblock ELSE braceblock
    var endifLabel = label();

    if(n.children.length === 5) {
        exprNodeCode(n.children[2]);
        emit("pop rax");
        emit("cmp rax, 0");
        emit(`je ${endifLabel}`);
        braceblockNodeCode(n.children[4]);
        emit(`${endifLabel}:`);
    }
    else if(n.children.length === 7) {
        var endelseLabel = label();
        exprNodeCode(n.children[2]);
        emit("pop rax");
        emit("cmp rax, 0");
        emit(`je ${endifLabel}`);
        braceblockNodeCode(n.children[4]);
        emit(`jmp ${endelseLabel}`);
        emit(`${endifLabel}:`);
        braceblockNodeCode(n.children[6]);
        emit(`${endelseLabel}:`);
    }
}

function loopNodeCode(n: TreeNode) {
    //loop -> WHILE LP expr RP braceblock;
    var startLabel = label();
    var exitLoopLabel = label();

    emit(`${startLabel}:`);
    exprNodeCode(n.children[2]);
    emit("pop rax");
    emit("cmp rax, 0");
    emit(`je ${exitLoopLabel}`);
    braceblockNodeCode(n.children[4]);
    emit(`jmp ${startLabel}`);
    emit(`${exitLoopLabel}:`);
}

function returnStmtNodeCode(n: TreeNode) {
    //return-stmt -> RETURN expr
    let exprType = exprNodeCode(n.children[1]);
    if(exprType === VarType.FLOAT){
        emit("movq xmm0, [rsp]");
        emit("add rsp, 8");
        //emit("roundsd xmm0, xmm0, 0xb");
        emit("cvtsd2si rax, xmm0");
    }
    else if(exprType === VarType.INTEGER)
        emit("pop rax");
    emit("ret");
}

function exprNodeCode(n: TreeNode ) : VarType {
    return orexpNodeCode(n.children[0]);
}

function orexpNodeCode(n: TreeNode ): VarType {
    // orexp -> orexp OR andexp | andexp
    if(n.children.length === 1){
        return andexpNodeCode(n.children[0]);
    }
    let c0_false = label();
    let orexpType = orexpNodeCode(n.children[0]);
    convertStackTopToZeroOrOneInteger(orexpType);
    emit("cmp qword [rsp], 0");
    emit(`jne ${c0_false}`);
    emit("add rsp, 8");
    let andexprType = andexpNodeCode(n.children[2]);
    convertStackTopToZeroOrOneInteger(andexprType);
    emit(`${c0_false}:`);
    return VarType.INTEGER;
}

function andexpNodeCode(n: TreeNode ) : VarType {
    // andexp -> andexp AND notexp | notexp
    if(n.children.length === 1)
    {
        return notexpNodeCode(n.children[0]);
    }
    let andexpType = andexpNodeCode(n.children[0]);
    let c0_false = label();

    convertStackTopToZeroOrOneInteger(andexpType);
    emit("cmp qword [rsp], 1");
    emit(`jne ${c0_false}`);
    emit("add rsp, 8");
    let notexpType = notexpNodeCode(n.children[2]);

    convertStackTopToZeroOrOneInteger(andexpType);
    emit(`${c0_false}:`);
    return VarType.INTEGER;
}

function notexpNodeCode(n: TreeNode ) : VarType {
    // notexp -> NOT notexp | rel
    if(n.children.length === 1){
        return relNodeCode(n.children[0]);
    }
    let notexpType = notexpNodeCode(n.children[1]);
    let end_zero = label();
    let end_one = label();
    
    convertStackTopToZeroOrOneInteger(notexpType);
    emit("cmp qword [rsp], 1");
    emit(`je ${end_zero}`);
    emit("pop rax");
    emit("push 1");
    emit(`jmp ${end_one}`);
    emit(`${end_zero}:`);
    emit("pop rax");
    emit("push 0");
    emit(`${end_one}:`);
    return VarType.INTEGER;
}    

function relNodeCode(n: TreeNode ): VarType {
    //rel |rarr| sum RELOP sum | sum
    if( n.children.length === 1 )
    {
        return sumNodeCode( n.children[0] );
    }
    else {
        let sum1Type = sumNodeCode( n.children[0] );
        let sum2Type = sumNodeCode( n.children[2] );

        if(sum1Type !== sum2Type){
            throw new Error("Types do not match.");
        }

        emit("pop rax");
        emit("cmp qword [rsp],rax");
        switch( n.children[1].token.lexeme ){
            case ">=":   emit("setge al"); break;
            case "<=":   emit("setle al"); break;
            case ">":    emit("setg  al"); break;
            case "<":    emit("setl  al"); break;
            case "==":   emit("sete  al"); break;
            case "!=":   emit("setne al"); break;
            default:     ICE()
        }
        emit("movzx qword rax, al");
        emit("mov [rsp], rax");
        return VarType.INTEGER;
    }
}

function sumNodeCode(n: TreeNode ): VarType {
    // sum -> sum PLUS term | sum MINUS term | term
    if(n.children.length === 1){
        return termNodeCode(n.children[0]);
    }
    else{
        let sumType = sumNodeCode(n.children[0]);
        let termType = termNodeCode(n.children[2]);

        if(sumType !== termType){
            throw new Error("Not all operands of " + n.children[1].sym + " are of type the same type.");
        }

        if(sumType === VarType.FLOAT){
            emit("movq xmm1, [rsp]");
            emit("add rsp, 8");
            emit("movq xmm0, [rsp]");
            emit("add rsp, 8");
        }
        else if(sumType === VarType.INTEGER){
            emit("pop rbx");
            emit("pop rax");
        }

        

        switch(n.children[1].sym){
            case "PLUS":
                if(sumType === VarType.FLOAT){
                    emit("addsd xmm0, xmm1");
                    break;
                }
                emit("add rax, rbx");
                break;
            case "MINUS":
                if(sumType === VarType.FLOAT){
                    emit("subsd xmm0, xmm1");
                    break;
                }
                emit("sub rax, rbx");
                break;
            default:
                ICE();
        }
        if(sumType === VarType.FLOAT){
            emit("sub rsp, 8");
            emit("movq [rsp], xmm0");
            return VarType.FLOAT;
        }
        emit("push rax");
        return VarType.INTEGER;
    }
}

function termNodeCode(n: TreeNode ): VarType {
    // term -> term MULOP neg | neg
    if(n.children.length === 1){
        return negNodeCode(n.children[0]);
    }
    else{
        let termType = termNodeCode(n.children[0]);
        let negType = negNodeCode(n.children[2]);

        if(termType !== negType){
            throw new Error("Type: " + termType.toString() + " does not match type: " + negType.toString());
        }

        switch(n.children[1].token.lexeme){
            case "*":
                if(termType === VarType.FLOAT){
                    emit("movq xmm1, [rsp]");
                    emit("add rsp, 8");
                    emit("movq xmm0, [rsp]");
                    emit("add rsp, 8");
                    emit("mulsd xmm0, xmm1");
                    emit("sub rsp, 8");
                    emit("movq [rsp], xmm0");
                    return VarType.FLOAT;
                }
                emit("pop rbx");
                emit("pop rax");
                emit("imul rbx");
                emit("push rax");
                return VarType.INTEGER;
            case "/":
                if(termType === VarType.FLOAT){
                    emit("movq xmm1, [rsp]");
                    emit("add rsp, 8");
                    emit("movq xmm0, [rsp]");
                    emit("add rsp, 8");
                    emit("divsd xmm0, xmm1");
                    emit("sub rsp, 8");
                    emit("movq [rsp], xmm0");
                    return VarType.FLOAT;
                }
                emit("pop rbx");
                emit("pop rax");
                emit("mov qword rdx, 0");
                emit("idiv rbx");
                emit("push rax");
                return VarType.INTEGER;
            case "%":
                if(termType === VarType.FLOAT){
                    throw new Error("Can't perform modulo operation on a float.");
                }
                emit("pop rbx");
                emit("pop rax");
                emit("mov qword rdx, 0");
                emit("idiv rbx");
                emit("push rdx");
                return VarType.INTEGER;
        }
    }
}

function negNodeCode(n: TreeNode ) : VarType {
    // neg -> MINUS neg | factor
    if(n.children.length === 1){
        return factorNodeCode(n.children[0])
    }

    let negType = negNodeCode(n.children[1]);

    if(negType === VarType.INTEGER){
        emit("pop rbx");
        emit("mov qword rax, 0");
        emit("sub rax, rbx");
        emit("push rax");
        return VarType.INTEGER;
    }
    else if(negType === VarType.FLOAT){
        emit("movq xmm1, [rsp]");
        emit("xorps xmm0, xmm0");
        emit("subsd xmm0, xmm1");
        emit("movq [rsp], xmm0");
        return VarType.FLOAT;
    }
    throw new Error("Reached end of neg");
}

function factorNodeCode(n: TreeNode ) : VarType {
    // factor -> NUM | FPNUM | LP expr RP | LP TYPE RP factor;
    let child = n.children[0];
    
    switch(child.sym){
        case "NUM":
            let i = parseInt(child.token.lexeme, 10);
            emit(`push qword ${i}`);
            return VarType.INTEGER;
        case "LP":
            if(n.children.length === 3)
                return exprNodeCode(n.children[1]);
            else if(n.children.length === 4){
                let cast = n.children[1].token.lexeme;
                let f_type = factorNodeCode(n.children[3]);
                if(cast === "int" && f_type === VarType.FLOAT){
                    emit("movq xmm0, [rsp]");
                    emit("add rsp, 8");
                    emit("roundsd xmm0, xmm0, 0xb");
                    emit("cvtsd2si rax, xmm0");
                    emit("push rax");
                    return VarType.INTEGER;
                }
                else if(cast === "double" && f_type === VarType.INTEGER){
                    emit("pop rax");
                    emit("cvtsi2sd xmm0, rax");
                    emit("sub rsp, 8");
                    emit("movq [rsp], xmm0");
                    return VarType.FLOAT;
                }
                else{
                    return f_type;
                }
                    
            }
        case "FPNUM":
            let f = parseFloat(child.token.lexeme);
            let f_str = f.toString();
            if(!f_str.includes('.')){
                f_str += '.0';
            }
            emit(`mov rax, __float64__(${f_str})`);
            emit("push rax");
            return VarType.FLOAT;
        case "STRINGCONST":
            let s_addr = stringconstantNodeCode(child);
            emit(`lea rcx, [${s_addr}]`);
            emit(`push rcx`);
            return VarType.STRING;
        case "ID":
            // TODO
            let variable = symTable.get(child.token.lexeme);            
            emit(`push qword [${variable.location}]`);
            return variable.type;
        case "funcCall":
            let type = funcCallNodeCode(n.children[0]);
            if(type === VarType.VOID) {
                throw new Error("Can't use void in expression");
            }
            emit("push rax");
            return type;
        default:
            ICE();
    }
}

function convertStackTopToZeroOrOneInteger(type: VarType){
    if( type == VarType.INTEGER ){
        emit("cmp qword [rsp], 0");
        emit("setne al");
        emit("movzx rax, al");
        emit("mov [rsp], rax");
    } else if(type === VarType.FLOAT){
        emit("xorpd xmm1, xmm1");
        emit("movq xmm0, [rsp]");
        emit("cmpneqsd xmm0, xmm1");
        emit("movq [rsp], xmm0");
        emit("and qword [rsp], 1");
    } else {
        throw new Error("Something is very wrong");
    }
}

function moveBytesFromStackToLocation(location: string) {
    emit("pop rax");
    emit(`mov [${location}], rax`);
}

function funcCallNodeCode(n: TreeNode): VarType {
    return builtInFuncCallNodeCode(n.children[0]);
}

function builtInFuncCallNodeCode( n: TreeNode ) : VarType {
    //builtin-func-call -> PRINT LP expr RP | INPUT LP RP |
    //OPEN LP expr RP | READ LP expr RP | WRITE LP expr CMA expr RP |
    //CLOSE LP expr RP
    switch( n.children[0].sym ){
        case "OPEN":
        {
            let type = exprNodeCode( n.children[2] );
            if( type !== VarType.STRING )
                throw new Error(`Open expected STRING but received: ${type}`);
            //tmp = fopen( filename, "a" );
            emit( "mov arg0, [rsp]");        //filename (string)
            emit( "mov arg1, string_a");    //next slide
            emit( "ffcall fopen" );
            //fclose(tmp)
            emit("mov arg0, rax")
            emit( "ffcall fclose" );
            //fopen( filename, "r+" )
            emit( "pop arg0" );        //filename; remove from stack
            emit( "mov arg1, string_rplus" ); //next slide
            emit( "ffcall fopen" );     //result is in rax
            return VarType.INTEGER;
        }
        case "CLOSE":
        {
            let type = exprNodeCode( n.children[2] );
            if( type !== VarType.INTEGER )
                throw new Error(`Close expected INTEGER but received: ${type}`);
            emit( "pop arg0" ); //argument for fclose
            emit( "ffcall fclose" );
            return VarType.VOID;
        }
        case "WRITE":
        {
            // WRITE LP expr CMA expr RP
            // fprintf( fp, "%s", str )  or  fprintf( fp, "%d", num )
            let handletype = exprNodeCode( n.children[2] );
            if( handletype !== VarType.INTEGER )
                throw new Error(`Write expected INTEGER but received: ${handletype}`);
            let outputtype = exprNodeCode( n.children[4] );
            let fmt: string;
            if( outputtype === VarType.INTEGER )
                fmt = "string_percent_d";
            else if( outputtype === VarType.STRING )
                fmt = "string_percent_s";
            else
                throw new Error("Can only write types INTEGER and STRING.");
            emit("pop arg2");   //the thing to print
            emit(`mov arg1, ${fmt}`);
            emit("pop arg0");   //the handle
            emit("ffvcall fprintf,0");
            //need to call fflush(NULL)
            emit("mov arg0, 0");
            emit("ffcall fflush");
            return VarType.VOID;
        }
        case "INPUT":
        {
            //INPUT LP RP
            //fgets( ptr, size, stream)
            //strtol( ptr, eptr, base )
            emit("mov arg0, fgets_buffer");
            emit("mov arg1, 64");
            emit("mov arg2, [stdin]");
            emit("ffcall fgets");
            //should do error checking...
            emit("mov arg0, fgets_buffer");
            emit("mov arg1, 0");
            emit("mov arg2, 10");
            emit("ffcall strtol");  //result is in rax
            return VarType.INTEGER;
        }
        case "PRINT":
        {
            let handletype = exprNodeCode(n.children[2]);
            if(handletype !== VarType.STRING && handletype !== VarType.INTEGER)
                throw new Error(`PRINT expected INTEGER or STRING but received: ${handletype}.`);
            let fmt: string;
            if( handletype === VarType.INTEGER )
                fmt = "string_percent_d";
            else if( handletype === VarType.STRING )
                fmt = "string_percent_s";
            else
                throw new Error("Can only write types INTEGER and STRING.");
            emit("pop arg1");
            emit(`mov arg0, ${fmt}`);
            emit("ffvcall printf,0");

            emit("mov arg0, 0");
            emit("ffcall fflush");
            return VarType.VOID;
        }
        case "READ":
        {
            let handletype = exprNodeCode(n.children[2]);
            if(handletype !== VarType.INTEGER)
                throw new Error("Arg for READ must be integer.");
            emit("mov arg0, fgets_buffer");
            emit("mov arg1, 64");
            emit(`pop arg2`)
            emit("ffcall fgets");

            emit("mov arg0, fgets_buffer");
            emit("mov arg1, 0");
            emit("mov arg2, 10");
            emit("ffcall strtol");
            return VarType.INTEGER;
        }
    }
}