export class Token {
    sym: string;
    lexeme: string;
    line: number;
    constructor(sym:string, lexeme:string, line:number){
        this.sym = sym;
        this.lexeme = lexeme;
        this.line = line;
    }
    toString(){
        // The following commented lines are pre-antlr4

        // let sym = this.sym.padStart(20,' ');
        // let line = ""+this.line;
        // line = line.padEnd(4,' ');
        // return `[${sym} ${line} ${this.lexeme}]`;

        return `${this.sym} ${this.line} ${this.lexeme}`
    }
}