import { NodeType } from './NodeType';

export class Grammar
{
    terminals : Array<[string, RegExp]> = new Array();
    nonterminals : Array<[string, string]> = new Array();

    productions: {[id: string]: string[]} = {}

    ids : Set<string> = new Set();
    defined : Set<string> = new Set();

    is_terminal : boolean = true;

    nullable: Set<string> = new Set();
    first: Map<string, Set<string>> = new Map();
    follow: Map<string, Set<string>> = new Map();

    constructor(spec : string)
    {
        let gram_split = spec.split('\n');

        gram_split.forEach(element => {
            if(element.length !== 0){
                if (this.is_terminal){
                    this.handle_terms(element);
                }
                else{
                    this.handle_non_terms(element);
                }
            }
            else{
                this.is_terminal = false;
            }
        });

        this.terminals.forEach(term =>{
            if (this.nonterminals.findIndex(nonterm => nonterm[0] === term[0]) != -1){
                throw new Error('Items cannot be both a terminal and nonterminal.');
            }
        });

        let used : Set<string> = new Set();
        let start_node : NodeType
        if(this.nonterminals.length !== 0){
            start_node = new NodeType(this.nonterminals[0][0]);
            this.dfs(start_node, used);
        }

        if(this.defined !== undefined){
            this.defined.forEach(def =>{
                if(!used.has(def))
                {
                    // TODO: Figure out why this breaks the nullable lab
                    // throw new Error(def + ' was defined but is not used.');
                }
            });
        }

        if (used !== undefined){
            used.forEach(u =>{
                if(u !== '' && !this.defined.has(u))
                {
                    throw new Error(u + ' is used but is not defined');
                }
            });
        }
        this.getNullable();
        this.getFirst();
    }

    handle_terms(element: string){
        if(!element.includes('->')){
            throw new Error('Syntax error');
        }
        let prod : Array<string> = element.split(' -> ');
        let id = prod[0];
        let rex = prod[1];
        if(!this.ids.has(id)){
            this.ids.add(id);
            this.defined.add(id);
        }
        else{
            throw new Error('Id: ' + id + ' already exists.');
        }

        try{
            let finRex: RegExp = new RegExp(rex);
            this.terminals.push([id, finRex]);
        }
        catch(e){
            throw new Error('Grammar: ' + rex + ' is not a valid regular expression.');
        }
    }

    handle_non_terms(element: string){
        let tmp_arr: Array<string> = element.split(' -> ');
        let tmp_lhs: string = tmp_arr[0];
        let tmp_rhs: string = tmp_arr[1].trim();
        let final_rhs: string[] = [];

        let found: number = this.nonterminals.findIndex(thing => thing[0] === tmp_lhs);

        if (found !== -1){
            let nonterm = this.nonterminals[found];
            this.nonterminals[found][1] = nonterm + ' | ' + tmp_rhs;
            tmp_rhs = this.nonterminals[found][1];
        }
        else{
            if(!this.defined.has(tmp_lhs)){
                this.defined.add(tmp_lhs);
            }
            this.nonterminals.push([tmp_lhs, tmp_rhs]);
        }

        tmp_rhs.split(new RegExp('\\|', 'g')).forEach(element => {
            let tmp_e: string = element.trim();
            if(tmp_e !== ''){
                final_rhs.push(tmp_e);
            }
        });
        this.productions[tmp_lhs] = final_rhs;
    }

    dfs(node: NodeType, used: Set<string>){
        used.add(node.label);
        const found = this.nonterminals.find(e => e[0] === node.label);
        if(found !== undefined)
        {
            let str = found[1];
            str = str.replace(new RegExp('\\|', 'g'), ' ');
            str = str.replace(new RegExp(',', 'g'), ' ');
            str.split(new RegExp(' ', 'g')).forEach(t =>{
                let tmp = t.trim();
                if(tmp !== '')
                {
                    if(tmp === 'lambda'){
                        tmp = '';
                    }
                    let new_node : NodeType = new NodeType(tmp);
                    node.n_type.push(new_node);
                }
            });
        }
        if(node.n_type !== undefined)
        {
            node.n_type.forEach((t: NodeType) => {
                if(!used.has(t.label)){
                    this.dfs(t, used);
                }
            });
        }
    }

    getNullable(): Set<string> {
        let nullable_added: boolean;
        while(true){
            nullable_added = false;
            this.nonterminals.forEach(nt =>{
                if(!this.nullable.has(nt[0])){
                    this.productions[nt[0]].forEach(p =>{
                        if(p.split(' ').every((sym: string) => this.nullable.has(sym) || sym === 'lambda')){
                            this.nullable.add(nt[0]);
                            nullable_added = true;
                        }
                    });
                }
            });
            if(!nullable_added)
                break;
        }
        return this.nullable;
    }

    getFirst(): Map<string, Set<string>>{
        let triggered: boolean;
        let passthrough: number = 0;
        this.terminals.forEach(terminal => {
            this.first.set(terminal[0], new Set<string>().add(terminal[0]));
        });
        while(true){
            triggered = false;
            this.nonterminals.forEach(nt =>{
                this.productions[nt[0]].forEach(p => {
                    let p_no_lam = p.replace('lambda', '');
                    let items: string[] = p_no_lam.split(' ');
                    for(let x in items){

                        let tmp0: Set<string> = (this.first.get(nt[0]) === undefined ? new Set() : this.first.get(nt[0]));
                        let tmp1: Set<string> = (this.first.get(items[x]) === undefined ? new Set() : this.first.get(items[x]));

                        tmp1.forEach(tmp0.add, tmp0);

                        let old_first = this.first;
                        this.first.set(nt[0], tmp0);
                        if(this.first.entries > old_first.entries)
                            triggered = true;

                        if(!this.nullable.has(items[x])){
                            break;
                        }
                    }
                });
            });
            if(!triggered){
                if(++passthrough > Object.keys(this.productions).length){
                    break;
                }
            }
        }
        return this.first;
    }

    getFollow(): Map<string, Set<string>> {
        this.follow.set(this.nonterminals[0][0], new Set<string>().add('$'));
        let passthrough: number = 0;
        while(true){
            let triggered: boolean = false;
            let is_nullable: boolean;
            let old_follow: Map<string, Set<string>>;
            this.nonterminals.forEach(nt => {
                this.productions[nt[0]].forEach(p => {
                    let p_split = p.split(' ');
                    for(let ti = 0; ti < p_split.length; ti++){
                        if(this.nonterminals.findIndex(element => element[0] === p_split[ti]) !== -1){
                            old_follow = this.follow;
                            is_nullable = true;
                            for(let y = ti + 1; y < p_split.length; y++){
                                let tmp0: Set<string> = (this.follow.get(p_split[ti]) === undefined ? new Set() : this.follow.get(p_split[ti]));
                                let tmp1: Set<string> = (this.first.get(p_split[y]) === undefined ? new Set() : this.first.get(p_split[y]));

                                tmp1.forEach(tmp0.add, tmp0);

                                this.follow.set(p_split[ti], tmp0);

                                if(!this.nullable.has(p_split[y])){
                                    is_nullable = false;
                                    break;
                                }
                            }

                            if(is_nullable){
                                let tmp0: Set<string> = (this.follow.get(nt[0]) === undefined ? new Set() : this.follow.get(nt[0]));
                                let tmp1: Set<string> = (this.follow.get(p_split[ti]) === undefined ? new Set() : this.follow.get(p_split[ti]));

                                tmp0.forEach(tmp1.add, tmp1);

                                this.follow.set(p_split[ti], tmp1);
                                is_nullable = false;
                            }
                            if(this.follow.entries > old_follow.entries){
                                triggered = true;
                            }
                        }
                    }
                });
            });

            if(!triggered){
                if(++passthrough >= Object.keys(this.productions).length)
                    break;
                triggered = false;
            }
        }
        return this.follow;
    }
}