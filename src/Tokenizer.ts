import {Token} from "./Token"
import {Grammar} from "./Grammar"

export class Tokenizer{
    grammar: Grammar;
    inputData: string;
    currentLine: number;
    idx: number;    //index of next unparsed char in inputData
    cur_tok: Token;
    previous_list:Token[] = [];

    constructor( grammar: Grammar ){
        this.grammar = grammar;
        let contains_whitespace: boolean = false;
        let contains_comment: boolean = false;
        this.grammar.terminals.forEach(element => {
            if (element[0] === "WHITESPACE"){
                contains_whitespace = true;
            }
            else if (element[0] === 'COMMENT'){
                contains_comment = true;
            }
        });
        if(!contains_whitespace){
            this.grammar.terminals.push(["WHITESPACE", new RegExp("\\s+")]);
        }
        if(!contains_comment){
            this.grammar.terminals.push(["COMMENT", new RegExp("/\\*(.|\\n)*?\\*/")]);
        }
    }
    setInput( inputData: string ){
        //...prepare for new parse...
        this.inputData = inputData;
        this.idx = 0;
        this.currentLine = 1;
    }
    next(): Token {

        if( this.idx >= this.inputData.length - 1){
            //special "end of file" metatoken
            return new Token("$",undefined,this.currentLine);
        }
        
        for(let i=0;i<this.grammar.terminals.length;++i){
            let terminal = this.grammar.terminals[i];
            let sym = terminal[0];
            let rex = new RegExp(terminal[1], 'y');     //RegExp
            rex.lastIndex = this.idx;  //tell where to start searching
            let m = rex.exec(this.inputData);   //do the search
            if( m ){
                //m[0] contains matched text as string
                let lexeme = m[0];
                this.idx += lexeme.length;
                let tmp_line = this.currentLine;
                this.currentLine += lexeme.split('\n').length - 1;
                if( sym !== "WHITESPACE" && sym !== "COMMENT" ){
                    //return new Token using sym, lexeme, and line number
                    this.cur_tok = new Token(sym, lexeme, tmp_line);
                    this.previous_list.push(this.cur_tok);
                    if(this.previous_list.length > 2){
                        this.previous_list.shift();
                    }
                    return this.cur_tok;
                } else {
                    //skip whitespace and get next real token
                    return this.next();
                }
            }
        }
        //no match; syntax error
        throw new Error("Syntax error, no match.");
    }

    previous(): Token{
        if(this.previous_list.length < 2){
            return undefined;
        }
        return this.previous_list[this.previous_list.length - 2];
    }

    peek(index: number = this.idx, line: number = this.currentLine): Token{
        if(index >= this.inputData.length - 1){
            return new Token("$", undefined, line);
        }
    
        for(let i = 0; i < this.grammar.terminals.length; i++){
            let terminal = this.grammar.terminals[i];
            let sym = terminal[0];
            let rex = new RegExp(terminal[1], 'y');
            rex.lastIndex = index;
            let m = rex.exec(this.inputData);
            if(m){
                let lexeme = m[0];
                index += lexeme.length;
                let tmp_line = line;
                line += lexeme.split('\n').length - 1;
    
                if(sym !== "WHITESPACE" && sym !== "COMMENT"){
                    return new Token(sym, lexeme, tmp_line);
                }
                else{
                    return this.peek(index, line);
                }
            }
        };
        throw new Error('No peek match');
    }
        
}