import { TreeNode } from "./NodeType";
import { Tokenizer } from "./Tokenizer";
import { Grammar } from "./Grammar";
import { Token } from "./Token";

let operators_prec: { [id: string] : number } = {
    "func-call": 6,
    "POWOP": 5,
    "NEGATE": 4,
    "BITNOT": 4,
    "MULOP": 3,
    "ADDOP": 2,
    "COMMA": 1,
    "LP": 0,
};

let operators_assoc: { [id: string] : string } = {
    "NEGATE": 'right',
    "BITNOT": 'right',
    "POWOP": 'right',
    "MULOP": 'left',
    "ADDOP": 'left',
    "COMMA": 'left',
    "func-call": 'left'
};

let arity: { [id: string] : number} = {
    "NEGATE": 1,
    "BITNOT": 1,
    "POWOP": 2,
    "MULOP": 2,
    "ADDOP": 2,
    "RP": 2,
    "LP": 2,
    "func-call": 2,
    "COMMA": 2,
};

let g_string =  'POWOP -> [*][*]\n' +
                'MULOP -> [*/]\n' +
                'BITNOT -> [~]\n' +
                'ADDOP -> [-+]\n' +
                'ASSIGNOP -> =(?!=)\n' +
                'NUM -> -?\\d+\n' +
                'SEMI -> [;]\n' +
                'IF -> \\bif\\b\n' +
                'ELSE -> \\belse\\b\n' +
                'LP -> [(]\n' +
                'RP -> [)]\n' +
                'COMMA -> [,]\n' +
                'ID -> [A-Za-z_]\\w*\n\n' +
                'S -> stmt\n' +
                'stmt -> cond | sum assign SEMI POWOP BITNOT COMMA\n' + //TODO: Write a real grammar
                'cond -> IF LP expr RP stmt | IF LP expr RP stmt ELSE stmt\n' +
                'assign -> ID ASSIGNOP expr\n' +
                'sum -> sum ADDOP product | product\n' +
                'product -> product MULOP pow | pow\n' +
                'pow -> factor POWOP pow | factor\n' +
                'expr -> expr ADDOP term | term\n' +
                'term -> term MULOP factor | factor\n' +
                'factor -> LP expr RP | NUM\n';
                

let grammar: Grammar = new Grammar(g_string);

let operandStack: TreeNode[] = [];
let operatorStack: TreeNode[] = [];

export function parse(input: string): TreeNode {
    operandStack = [];
    operatorStack = [];
    let tokenizer: Tokenizer = new Tokenizer(grammar);
    tokenizer.setInput(input);
    while(true) {
        let t: Token = tokenizer.next();

        if(t.sym === '$'){
            break;
        }

        if(t.lexeme === '-'){
            let p: Token = tokenizer.previous();
            if(p === undefined || p.sym === 'LP' || operators_prec[p.sym] !== undefined){
                t.sym = 'NEGATE';
            }
        }

        if(t.sym === 'LP'){
            if(tokenizer.previous() !== undefined && tokenizer.previous().sym === 'ID'){
                let peek_tok = tokenizer.peek();
                if(peek_tok !== undefined && peek_tok.sym === "RP"){
                    arity['func-call'] = 1;
                    operatorStack.push(new TreeNode('func-call', undefined));
                    tokenizer.next(); // Get rid of RP, so it doesn't cause problems without the LP
                    doOperation();
                    continue;
                }
                arity["func-call"] = 2;
                operatorStack.push(new TreeNode('func-call', undefined));
            }
            operatorStack.push(new TreeNode(t.sym, t));
        }
        else if(arity[t.sym] === 1 && operators_assoc[t.sym] === 'right'){
            operatorStack.push(new TreeNode(t.sym, t));
        }
        else if(t.sym === 'NUM' || t.sym === 'ID'){
            operandStack.push(new TreeNode(t.sym, t));
        }
        
        else if(t.sym === 'RP')
        {
            while(true){

                let top = operatorStack[operatorStack.length - 1];
                if(top !== undefined && top.sym === 'LP'){
                    operatorStack.pop();
                    break;
                }
                else if(top === undefined){
                    break;
                }

                doOperation();
            }
        }
        else{
            let assoc = operators_assoc[t.sym];
            while(true){
                if(operatorStack.length === 0){
                    break;
                }

                let A = operatorStack[operatorStack.length -1].sym;
                if(assoc === 'right' && operators_prec[A] > operators_prec[t.sym]){
                    doOperation();
                }
                else if(assoc === 'left' && operators_prec[A] >= operators_prec[t.sym]){
                    doOperation();
                }
                else{
                    break;
                }
            }
            operatorStack.push(new TreeNode(t.sym, t));
        }
    }
    while(operatorStack.length !== 0) {
        doOperation();
    }
    return operandStack[0];
}

function doOperation() {
    let opNode: TreeNode = operatorStack.pop();
    let c1: TreeNode = operandStack.pop();
    if(arity[opNode.sym] === 2){
        let c2 = operandStack.pop();
        opNode.addChild(c2);
    }
    else if(opNode.sym === 'func-call'){
        arity['func-call'] = 2;
    }
    opNode.addChild(c1);
    operandStack.push(opNode);
}